import 'package:flutter/material.dart';

class HomeThumbWidget extends StatelessWidget {
  final String? image, label;
  const HomeThumbWidget({Key? key, required this.image, this.label})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 5,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        color: Colors.orange,
        width: 100,
        height: 100,
        child: Column(
          children: [
            Image.asset(
              image!,
            ),
            Text(label!),
          ],
        ),
      ),
    );
  }
}
