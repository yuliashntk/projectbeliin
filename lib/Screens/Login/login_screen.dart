import 'package:argon_buttons_flutter/argon_buttons_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter_auth/bloc/login_bloc.dart';
import 'package:flutter_auth/components/text_field_container.dart';
import 'package:flutter_auth/conf/shared_pref.dart';
import 'package:flutter_auth/response/login_response.dart';
import 'package:flutter_auth/screens/home/main_screen.dart';
import 'package:flutter_auth/screens/signup/signup_screen.dart';
import 'package:flutter_auth/components/rounded_input_field.dart';
import 'package:flutter_auth/components/rounded_password_field.dart';
import 'package:flutter_auth/constants.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class LoginScreen extends StatefulWidget {
  LoginScreen({Key? key}) : super(key: key);

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _loginForm = GlobalKey<FormState>();
  TextEditingController emailController = new TextEditingController();
  TextEditingController passwordController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final username = TextFieldContainer(
      child: TextFormField(
        validator: (value) {
          if (value!.isEmpty) {
            return "Data tidak boleh kosong";
          }
        },
      ),
    );

    final password = RoundedPasswordField(
      controller: passwordController,
    );

    return Scaffold(
      body: Container(
        width: double.infinity,
        height: size.height,
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Positioned(
              top: 0,
              left: 0,
              child: Image.asset(
                "assets/images/main_top.png",
                width: size.width * 0.35,
              ),
            ),
            Positioned(
              bottom: 0,
              right: 0,
              child: Image.asset(
                "assets/images/login_bottom.png",
                width: size.width * 0.4,
              ),
            ),
            SingleChildScrollView(
              child: Form(
                key: _loginForm,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "MASUK",
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                    Image.asset(
                      "assets/images/img-login.png",
                      width: size.width * 0.50,
                    ),
                    SizedBox(height: size.height * 0.03),
                    RoundedInputField(
                      controller: emailController,
                      hintText: "Masukkan Email Anda",
                    ),
                    password,
                    StreamBuilder<LoginResponse?>(
                        stream: loginBloc.subject.stream,
                        builder:
                            (context, AsyncSnapshot<LoginResponse?> snapshot) {
                          if (snapshot.hasData) {
                            if (snapshot.data?.message != null) {
                              WidgetsBinding.instance!
                                  .addPostFrameCallback((timeStamp) {
                                Alert(
                                  context: context,
                                  title: "INFO",
                                  desc: snapshot.data!.message!
                                          .contains("Instance")
                                      ? "Please check internet connections"
                                      : snapshot.data!.message,
                                  buttons: [
                                    DialogButton(
                                      child: Text(
                                        "OK",
                                        style: TextStyle(
                                            color: Colors.white, fontSize: 20),
                                      ),
                                      onPressed: () => Navigator.pop(context),
                                      width: 120,
                                    ),
                                  ],
                                ).show();
                                loginBloc.drainStream();
                              });
                            }

                            if (snapshot.data?.user != null) {
                              SharedPref.setUserId(
                                  snapshot.data!.user!.id.toString());

                              SharedPref.setEmail(
                                  snapshot.data!.user!.email.toString());
                              SharedPref.setFullname(
                                  snapshot.data!.user!.name.toString());
                              SharedPref.setPhone(
                                  snapshot.data!.user!.phone.toString());
                              SharedPref.setToken(snapshot
                                  .data!.tokens!.access!.token
                                  .toString());
                              WidgetsBinding.instance!
                                  .addPostFrameCallback((timeStamp) {
                                loginBloc.drainStream();
                                Navigator.pushReplacement(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) => MainScreen(),
                                  ),
                                );
                              });
                              return Container();
                            }
                          } else if (snapshot.hasError) {
                            WidgetsBinding.instance!
                                .addPostFrameCallback((timeStamp) {
                              Alert(
                                context: context,
                                title: "INFO",
                                desc:
                                    snapshot.data!.message!.contains("Instance")
                                        ? "Please check internet connections"
                                        : snapshot.data!.message,
                                buttons: [
                                  DialogButton(
                                    child: Text(
                                      "OK",
                                      style: TextStyle(
                                          color: Colors.white, fontSize: 20),
                                    ),
                                    onPressed: () => Navigator.pop(context),
                                    width: 120,
                                  ),
                                ],
                              ).show();
                              loginBloc.drainStream();
                            });
                          }

                          return Center(
                            child: Padding(
                              padding: const EdgeInsets.all(10.0),
                              child: Container(
                                margin: EdgeInsets.symmetric(vertical: 10),
                                width: size.width * 0.8,
                                child: ClipRRect(
                                  borderRadius: BorderRadius.circular(29),
                                  child: ArgonButton(
                                    height: 50,
                                    width: MediaQuery.of(context).size.width,
                                    borderRadius: 5.0,
                                    color: kPrimaryColor,
                                    child: Text(
                                      "MASUK",
                                      style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 18,
                                          fontWeight: FontWeight.w700),
                                    ),
                                    loader: Container(
                                      padding: EdgeInsets.all(10),
                                      child: SpinKitCircle(
                                        color: Colors.white,
                                        // size: loaderWidth ,
                                      ),
                                    ),
                                    onTap: (startLoading, stopLoading,
                                        btnState) async {
                                      if (btnState == ButtonState.Idle) {
                                        if (_loginForm.currentState!
                                            .validate()) {
                                          startLoading();
                                          // If the form is valid, display a snackbar. In the real world,
                                          // you'd often call a server or save the information in a database.
                                          // print(emailController.text);
                                          await loginBloc.doLogin(
                                              emailController.text,
                                              passwordController.text);

                                          stopLoading();
                                        }
                                      } else {
                                        print("buttons sedang idle");
                                      }
                                    },
                                  ),
                                ),
                              ),
                            ),
                          );
                        }),
                    SizedBox(height: size.height * 0.03),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          "Belum Memiliki Akun ? ",
                          style: TextStyle(color: kPrimaryColor),
                        ),
                        GestureDetector(
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) {
                                  return SingupScreen();
                                },
                              ),
                            );
                          },
                          child: Text(
                            "Daftar",
                            style: TextStyle(
                              color: kPrimaryColor,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        )
                      ],
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
