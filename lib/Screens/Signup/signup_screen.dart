import 'package:flutter/material.dart';
import 'package:flutter_auth/components/rounded_input_field.dart';
import 'package:flutter_auth/components/rounded_inputaddress_field.dart';
import 'package:flutter_auth/components/rounded_inputtgl_field.dart';
import 'package:flutter_auth/components/rounded_inputtlpn_field.dart';
import 'package:flutter_auth/components/rounded_password_field.dart';
import 'package:flutter_auth/constants.dart';
import 'package:flutter_auth/screens/login/login_screen.dart';

class SingupScreen extends StatefulWidget {
  SingupScreen({Key? key}) : super(key: key);

  @override
  _SingupScreenState createState() => _SingupScreenState();
}

class _SingupScreenState extends State<SingupScreen> {
  TextEditingController controller = new TextEditingController();
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      body: Container(
        height: size.height,
        width: double.infinity,
        // Here i can use size.width but use double.infinity because both work as a same
        child: Stack(
          alignment: Alignment.center,
          children: <Widget>[
            Positioned(
              top: 0,
              left: 0,
              child: Image.asset(
                "assets/images/signup_top.png",
                width: size.width * 0.35,
              ),
            ),
            Positioned(
              bottom: 0,
              left: 0,
              child: Image.asset(
                "assets/images/main_bottom.png",
                width: size.width * 0.25,
              ),
            ),
            SingleChildScrollView(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    "DAFTAR",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  Image.asset(
                    "assets/images/img-register.png",
                    width: size.width * 0.50,
                  ),
                  SizedBox(height: size.height * 0.03),
                  RoundedInputTeleponField(
                    hintText: "Masukkan Nomor Hp/Whatsapp",
                    onChanged: (value) {},
                  ),
                  RoundedInputField(
                    hintText: "Masukkan Nama Lengkap Anda",
                    onChanged: (value) {},
                  ),
                  RoundedInputTglUltahField(
                    hintText: "Masukkan Tgl Ulang Tahun Anda",
                    onChanged: (value) {},
                  ),
                  RoundedInputAddressField(
                    hintText: "Masukkan Alamat Anda",
                    onChanged: (value) {},
                  ),
                  RoundedInputField(
                    hintText: "Masukkan Email Anda",
                    onChanged: (value) {},
                  ),
                  RoundedPasswordField(
                    onChanged: (value) {},
                  ),
                  Text(
                    "*catatan: pastikan nomor anda dapat dihubungi serta aktif whatsapp, jika tidak aktif, maka secara otomatis kamu akan dibatalkan menjadi pemenang hadiah Kue Ulang Tahun Gratis!",
                  style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  
                  Container(
                    margin: EdgeInsets.symmetric(vertical: 10),
                    width: size.width * 0.8,
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(29),
                      child: ElevatedButton(
                        child: Text(
                          "DAFTAR",
                          style: TextStyle(color: Colors.white),
                        ),
                        onPressed: () {},
                        style: ElevatedButton.styleFrom(
                            primary: kPrimaryColor,
                            padding: EdgeInsets.symmetric(
                                horizontal: 40, vertical: 20),
                            textStyle: TextStyle(
                                color: Colors.white,
                                fontSize: 14,
                                fontWeight: FontWeight.w500)),
                      ),
                    ),
                  ),
                  SizedBox(height: size.height * 0.03),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        "Sudah Memiliki Akun ? ",
                        style: TextStyle(color: kPrimaryColor),
                      ),
                      GestureDetector(
                        onTap: () {
                          Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) {
                                return LoginScreen();
                              },
                            ),
                          );
                        },
                        child: Text(
                          "Masuk",
                          style: TextStyle(
                            color: kPrimaryColor,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      )
                    ],
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
