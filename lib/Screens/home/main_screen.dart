import 'package:flutter/material.dart';
import 'package:flutter_auth/conf/shared_pref.dart';
import 'package:flutter_auth/screens/home/homepage_screen.dart';
import 'package:flutter_auth/screens/welcome/welcome_screen.dart';
import 'package:flutter_auth/screens/widget/home_thumb_widget.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  @override
  Widget build(BuildContext context) {
    Widget searchBox() {
      return Padding(
        padding: EdgeInsets.symmetric(horizontal: 20),
        child: Row(
          children: [
            Expanded(
              child: Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  height: 50,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(100),
                      border: Border.all(width: 1.5, color: Colors.grey)),
                  child: Row(
                    children: [
                      Icon(Icons.search, color: Colors.black, size: 40),
                      SizedBox(width: 10),
                      Expanded(
                          child: Text(
                        "Search",
                        style: TextStyle(color: Colors.grey, fontSize: 12),
                      ))
                    ],
                  )),
            ),
            SizedBox(width: 20),
            // ClipRRect(
            //   borderRadius: BorderRadius.circular(1000),
            //   child: Image.asset(
            //     "assets/images/avatar.png",
            //     height: 55,
            //     width: 55,
            //     fit: BoxFit.cover,
            //   ),
            // )
            Icon(Icons.person),
          ],
        ),
      );
    }

    return SingleChildScrollView(
      child: Padding(
        padding: EdgeInsets.symmetric(horizontal: 8, vertical: 8 * 3),
        child: Column(
          children: [
            searchBox(),
            Container(
              alignment: Alignment.bottomLeft,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    'Beliin Yuks!',
                    style: TextStyle(
                        fontSize: 37,
                        fontWeight: FontWeight.bold,
                        color: Colors.black),
                  ),
                ],
              ),
            ),
            SizedBox(height: 25),
            Container(
              alignment: Alignment.bottomLeft,
              child: Text(
                'Jajanan Murah',
                style: TextStyle(fontSize: 30, color: Colors.black),
              ),
            ),
            SizedBox(height: 40),
            Container(
              height: 140,
              // width: MediaQuery.of(context).size.width,
              child: ListView(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                children: [
                  HomeThumbWidget(
                      image: 'assets/images/img-register.png', label: "TEST"),
                  HomeThumbWidget(
                      image: 'assets/images/img-register.png', label: "TEST"),
                  HomeThumbWidget(
                      image: 'assets/images/img-register.png', label: "TEST"),
                  HomeThumbWidget(
                      image: 'assets/images/img-register.png', label: "TEST"),
                  HomeThumbWidget(
                      image: 'assets/images/img-register.png', label: "TEST"),
                  HomeThumbWidget(
                      image: 'assets/images/img-register.png', label: "TEST"),

                  // InkWell(
                  //   onTap: () {
                  //     // Navigator.push(
                  //     //     context,
                  //     //     MaterialPageRoute(
                  //     //         builder: (c) => LayananDetailsPage()));
                  //   },
                  //   child: ListViewCard(
                  //     img: 'images/hibirth.png',
                  //     label: 'Cimol Limbangan',
                  //   ),
                  // ),
                  // ListViewCard(
                  //     img: 'images/paketdeliv.png',
                  //     label: 'Batagor',
                  //   ),
                  // ListViewCard(
                  //     img: 'images/paketdeliv.png',
                  //     label: 'Pempek',
                  // ),
                ],
              ),
            ),
            SizedBox(height: 25),
            Container(
              alignment: Alignment.bottomLeft,
              child: Text(
                'Jastip Beliin',
                style: TextStyle(fontSize: 30, color: Colors.black),
              ),
            ),
            SizedBox(height: 25),
            Container(
              height: 140,
              // width: MediaQuery.of(context).size.width,
              child: ListView(
                scrollDirection: Axis.horizontal,
                shrinkWrap: true,
                children: [
                  HomeThumbWidget(
                      image: 'assets/images/img-register.png', label: "TEST"),
                  HomeThumbWidget(
                      image: 'assets/images/img-register.png', label: "TEST"),
                  HomeThumbWidget(
                      image: 'assets/images/img-register.png', label: "TEST"),
                  HomeThumbWidget(
                      image: 'assets/images/img-register.png', label: "TEST"),
                  HomeThumbWidget(
                      image: 'assets/images/img-register.png', label: "TEST"),
                  HomeThumbWidget(
                      image: 'assets/images/img-register.png', label: "TEST"),

                  // InkWell(
                  //   onTap: () {
                  //     // Navigator.push(
                  //     //     context,
                  //     //     MaterialPageRoute(
                  //     //         builder: (c) => LayananDetailsPage()));
                  //   },
                  //   child: ListViewCard(
                  //     img: 'images/hibirth.png',
                  //     label: 'Cimol Limbangan',
                  //   ),
                  // ),
                  // ListViewCard(
                  //     img: 'images/paketdeliv.png',
                  //     label: 'Batagor',
                  //   ),
                  // ListViewCard(
                  //     img: 'images/paketdeliv.png',
                  //     label: 'Pempek',
                  // ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
