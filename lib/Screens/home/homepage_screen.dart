import 'package:flutter/material.dart';
import 'package:flutter_auth/constants.dart';
import 'package:flutter_auth/screens/home/delivery_screen.dart';
import 'package:flutter_auth/screens/home/main_screen.dart';
import 'package:flutter_auth/screens/home/promo_screen.dart';

class HomepageScreen extends StatefulWidget {
  HomepageScreen({Key? key}) : super(key: key);

  @override
  _HomepageScreenState createState() => _HomepageScreenState();
}

class _HomepageScreenState extends State<HomepageScreen>
    with SingleTickerProviderStateMixin {
  var _scrollController = ScrollController();
  PageController _pageController = new PageController();

  @override
  void initState() {
    super.initState();
  }

  List<bool> tabBarBadgeList = [
    false,
    false,
    false,
  ];

  List tabBarList = [
    "Home",
    "Promo",
    "Delivery",
  ];

  int tabBarIndex = 0;

  int balanceBalance = 0;

  bool isBrush = false;

  bool isCollapseNavBottom = true;

  @override
  Widget build(BuildContext context) {
    Widget tabBarItem(int index) {
      ///

      ///
      return Expanded(
        child: Stack(
          children: [
            Container(
                margin: EdgeInsets.all(5),
                height: double.infinity,
                decoration: BoxDecoration(
                    color: (tabBarIndex == index)
                        ? Colors.white
                        : Colors.transparent,
                    borderRadius: BorderRadius.circular(100)),
                child: InkWell(
                  onTap: () {
                    setState(() {
                      tabBarIndex = index;
                      _pageController.animateToPage(index,
                          duration: const Duration(milliseconds: 100),
                          curve: Curves.ease);
                    });
                  },
                  child: Center(
                    child: Text(
                      tabBarList[index],
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: (tabBarIndex == index)
                              ? Colors.orange
                              : Colors.white,
                          fontSize: 12,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                )),
            if (tabBarBadgeList[index])
              Align(
                alignment: Alignment.topRight,
                child: Container(
                    height: 20,
                    width: 20,
                    decoration: BoxDecoration(
                        color: Colors.red,
                        borderRadius: BorderRadius.circular(100),
                        border: Border.all(width: 1.5, color: Colors.white)),
                    child: Center(
                      child: Container(
                        height: 4,
                        width: 4,
                        decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(100),
                        ),
                      ),
                    )),
              )
          ],
        ),
      );
    }

    Widget tabBar() {
      return Container(
        margin: EdgeInsets.only(bottom: 10, top: 10),
        height: 40,
        width: double.infinity,
        decoration: BoxDecoration(
            color: Colors.red, borderRadius: BorderRadius.circular(100)),
        child: Row(
          children: [
            tabBarItem(0),
            tabBarItem(1),
            tabBarItem(2),
          ],
        ),
      );
    }

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: tabBar(),
      ),
      /*body: Stack (
        children: [
          ListView(
            controller: _scrollController,
            children: [
              SizedBox(height: 20),
              searchBox(),
              SizedBox(height: 20),
            ],
          ), 
        ]
        
        
      ), */

      body: PageView(
        controller: _pageController,
        children: [
          new MainScreen(),
          new PromoScreen(),
          new DeliveryScreen(),
        ],
      ),
    );
  }
}
