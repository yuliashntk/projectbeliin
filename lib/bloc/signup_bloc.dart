
import 'package:flutter/material.dart';
import 'package:flutter_auth/repository/signup_repository.dart';
import 'package:flutter_auth/response/signup_response.dart';

import 'package:rxdart/subjects.dart';

class SignupBloc {
  final SignupRepository _repository = SignupRepository();
  final BehaviorSubject<SignupResponse?> _subject =
      BehaviorSubject<SignupResponse?>();

  doSignup(
    String? nohp,
    String? namalengkap,
    String? tglultah,
    String? alamat,
    String? email,
    String? password,
  ) async {
    SignupResponse? response = await _repository.signup(nohp, namalengkap, tglultah, alamat, email, password);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<SignupResponse?> get subject => _subject;
}
