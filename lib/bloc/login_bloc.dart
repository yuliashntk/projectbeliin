import 'package:flutter_auth/repository/login_repository.dart';
import 'package:flutter_auth/response/login_response.dart';
import 'package:flutter/material.dart';

import 'package:rxdart/subjects.dart';

class LoginBloc {
  final LoginRepository _repository = LoginRepository();
  final BehaviorSubject<LoginResponse?> _subject =
      BehaviorSubject<LoginResponse?>();

  doLogin(
    String? email,
    String? password,
  ) async {
    LoginResponse? response = await _repository.login(email, password);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<LoginResponse?> get subject => _subject;
}

final loginBloc = LoginBloc();
