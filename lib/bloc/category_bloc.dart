

import 'package:flutter/material.dart';
import 'package:flutter_auth/repository/category_repository.dart';
import 'package:flutter_auth/response/category_response.dart';

import 'package:rxdart/subjects.dart';

class CategoryBloc {
  final CategoryRepository _repository = CategoryRepository();
  final BehaviorSubject<CategoryResponse?> _subject =
      BehaviorSubject<CategoryResponse?>();

  doLogin(
    String? email,
    String? password,
  ) async {
    CategoryResponse? response = await _repository.login(email, password);
    _subject.sink.add(response);
  }

  void drainStream() {
    _subject.value = null;
  }

  @mustCallSuper
  void dispose() async {
    await _subject.drain();
    _subject.close();
  }

  BehaviorSubject<CategoryResponse?> get subject => _subject;
}

