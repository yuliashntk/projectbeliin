import 'package:flutter/material.dart';
import 'package:flutter_auth/components/text_field_container.dart';
import 'package:flutter_auth/constants.dart';
import 'package:date_time_picker/date_time_picker.dart';
class RoundedInputTglUltahField extends StatelessWidget {
  final String? hintText;
  final IconData icon;
  final ValueChanged<String>? onChanged;
  const RoundedInputTglUltahField({
    Key? key,
    this.hintText,
    this.icon = Icons.calendar_today,
    this.onChanged,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return TextFieldContainer(
      child: DateTimePicker(
          initialValue: '',
          firstDate: DateTime(1985),
          lastDate: DateTime(2022),
          decoration: InputDecoration(
            icon: Icon(
              icon,
              color: kPrimaryColor,
              ),
              hintText: hintText,
              border: InputBorder.none,
          ),
          onChanged: (val) => print(val),
          validator: (val) {
            print(val);
            return null;
          },
            onSaved: (val) => print(val),
          ), //(
          //onChanged: onChanged,
          //cursorColor: kPrimaryColor,
         // decoration: InputDecoration(
            //icon: Icon(
            //  icon,
             // color: kPrimaryColor,
           // ),
           // hintText: hintText,
          ///  border: InputBorder.none,
        //  ),
        );
    //  );
  }
}
