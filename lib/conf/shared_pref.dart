import 'package:flutter_auth/helper/pref_helper.dart';

class SharedPref {
  static const String FULLNAME = "FULLNAME";
  static const String EMAIL = "EMAIL";
  static const String PHONE = "PHONE";
  static const String ADDRESS = "ADDRESS";
  static const String ROLE = "ROLE";
  static const String TOKEN = "TOKEN";
  static const String FCMTOKEN = "FCMTOKEN";
  static const String REFRESH = "REFRESH";
  static const String STOREID = "STOREID";
  static const String USERID = "USERID";

  static Future<String> get getFullname =>
      PreferencesHelper.getString(FULLNAME);

  static Future setFullname(String value) =>
      PreferencesHelper.setString(FULLNAME, value);

  static Future<String> get getUserId => PreferencesHelper.getString(USERID);

  static Future setUserId(String value) =>
      PreferencesHelper.setString(USERID, value);

  static Future<String> get getEmail => PreferencesHelper.getString(EMAIL);

  static Future setEmail(String value) =>
      PreferencesHelper.setString(EMAIL, value);

  static Future<String> get getPhone => PreferencesHelper.getString(PHONE);

  static Future setPhone(String value) =>
      PreferencesHelper.setString(ADDRESS, value);

  static Future<String> get getAddress => PreferencesHelper.getString(ADDRESS);

  static Future setAddress(String value) =>
      PreferencesHelper.setString(PHONE, value);

  static Future<String> get getRole => PreferencesHelper.getString(ROLE);

  static Future setRole(String value) =>
      PreferencesHelper.setString(ROLE, value);

  static Future<String> get getToken => PreferencesHelper.getString(TOKEN);

  static Future setToken(String value) =>
      PreferencesHelper.setString(TOKEN, value);

  static Future<String> get getFcmToken =>
      PreferencesHelper.getString(FCMTOKEN);

  static Future setFcmToken(String value) =>
      PreferencesHelper.setString(FCMTOKEN, value);

  static Future<String> get getStoreid => PreferencesHelper.getString(STOREID);

  static Future setStoreid(String value) =>
      PreferencesHelper.setString(STOREID, value);

  static Future<String> get getRefreshToken =>
      PreferencesHelper.getString(REFRESH);

  static Future setRefreshToken(String value) =>
      PreferencesHelper.setString(REFRESH, value);

  static Future<void> clear() async {
    await Future.wait(<Future>[
      setFullname(''),
      setRole(''),
      setUserId(''),
      setAddress(''),
      setEmail(''),
      setPhone(''),
      setRefreshToken(''),
      setStoreid(''),
    ]);
  }
}
