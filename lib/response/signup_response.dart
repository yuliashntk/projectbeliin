import 'package:flutter_auth/model/tokens.dart';
import 'package:flutter_auth/model/user.dart';

class SignupResponse {
  final User? user;
  final String? message;
  final Tokens? tokens;

  SignupResponse(this.user, this.message, this.tokens);

  SignupResponse.fromJson(Map<String, dynamic> json)
      : user = User.fromJson(json['user']),
        tokens = Tokens.fromJson(json['tokens']),
        message = null;

  SignupResponse.withError(String message)
      : user = null,
        tokens = null,
        message = message;
}
