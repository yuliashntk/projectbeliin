import 'package:flutter_auth/model/tokens.dart';
import 'package:flutter_auth/model/user.dart';

class CategoryResponse {
  final User? user;
  final String? message;
  final Tokens? tokens;

  CategoryResponse(this.user, this.message, this.tokens);

  CategoryResponse.fromJson(Map<String, dynamic> json)
      : user = User.fromJson(json['user']),
        tokens = Tokens.fromJson(json['tokens']),
        message = null;

  CategoryResponse.withError(String message)
      : user = null,
        tokens = null,
        message = message;
}
