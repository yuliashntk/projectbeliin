import 'package:flutter_auth/model/user.dart';

class UserResponse {
  final User? user;
  final String? message;

  UserResponse(this.user, this.message);

  UserResponse.fromJson(Map<String, dynamic> json)
      : user = User.fromJson(json),
        message = null;

  UserResponse.withError(String message)
      : user = null,
        message = message;
}
