import 'package:flutter_auth/model/tokens.dart';
import 'package:flutter_auth/model/user.dart';

class LoginResponse {
  final User? user;
  final String? message;
  final Tokens? tokens;

  LoginResponse(this.user, this.message, this.tokens);

  LoginResponse.fromJson(Map<String, dynamic> json)
      : user = User.fromJson(json['user']),
        tokens = Tokens.fromJson(json['tokens']),
        message = null;

  LoginResponse.withError(String message)
      : user = null,
        tokens = null,
        message = message;
}
