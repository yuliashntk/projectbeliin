
import 'package:dio/dio.dart';
import 'package:flutter_auth/conf/global.dart' as Global;
import 'package:flutter_auth/response/signup_response.dart';

class SignupRepository {
  static String userUrl = Global.userUrl;
  static String apiUrl = Global.apiUrl;

  final Dio dio = Dio();
  var token;
  var user;
  var registerUrl = '$apiUrl/auth/register';
  var loginUrl = '$apiUrl/auth/login';
  var logoutUrl = '$apiUrl/user/logout';

  Future<SignupResponse> signup(String? nohp, String? namalengkap, String? tglultah, String? alamat, String? email, String? password) async {
    try {
      Response response = await dio.post(
        '$registerUrl',
        data: {"nohp": nohp, "namalengkap": namalengkap, "tglultah": tglultah, "alamat": alamat, "email": email, "password": password},
        options: Options(
          contentType: Headers.formUrlEncodedContentType,
          followRedirects: false,
          validateStatus: (status) {
            return status! < 500;
          },
        ),
      );

      if (response.statusCode == 200) {
        return SignupResponse.fromJson(response.data);
      }

      return SignupResponse.withError(response.data['message']);
    } on DioError catch (error) {
      return SignupResponse.withError(error.message);
    }
  }
  Future<SignupResponse> login(
    String? email,
    String? password,
   
  ) async {
    try {
      Response response = await dio.post(
        '$loginUrl',
        data: {
          "email": email,
        //  "phone": phone,
         // "name": name,
          "password": password,
        },
        options: Options(
          contentType: Headers.formUrlEncodedContentType,
          followRedirects: false,
          validateStatus: (status) {
            return status! < 500;
          },
        ),
      );

      if (response.statusCode == 200 || response.statusCode == 201) {
        return SignupResponse.fromJson(response.data);
      }

      return SignupResponse.withError(response.data['message']);
    } on DioError catch (error) {
      return SignupResponse.withError(error.message);
    }
  }

  // Future<ApiResponse> logout() async {
  //   await SharedPref.getToken.then((value) => {token = value});
  //   await SharedPref.getStoreid.then((value) => {storeId = value});
  //   var email;
  //   await SharedPref.getEmail.then((value) => {email = value});
  //   try {
  //     Response response = await dio.post(
  //       '$logoutUrl/$storeId/$token',
  //       data: {
  //         "email": email,
  //       },
  //       options: Options(contentType: Headers.formUrlEncodedContentType),
  //     );

  //     return ApiResponse.fromJson(response.data);
  //   } on DioError catch (error) {
  //     return ApiResponse.withError(error.requestOptions.toString());
  //   }
  // }
}
