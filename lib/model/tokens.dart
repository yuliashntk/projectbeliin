import 'access.dart';
import 'refresh.dart';

class Tokens {
	Access? access;
	Refresh? refresh;

	Tokens({this.access, this.refresh});

	@override
	String toString() => 'Tokens(access: $access, refresh: $refresh)';

	factory Tokens.fromJson(Map<String, dynamic> json) => Tokens(
				access: json['access'] == null
						? null
						: Access.fromJson(json['access'] as Map<String, dynamic>),
				refresh: json['refresh'] == null
						? null
						: Refresh.fromJson(json['refresh'] as Map<String, dynamic>),
			);

	Map<String, dynamic> toJson() => {
				'access': access?.toJson(),
				'refresh': refresh?.toJson(),
			};

	@override
	bool operator ==(Object other) {
		if (identical(other, this)) return true;
		return other is Tokens && 
				other.access == access &&
				other.refresh == refresh;
	}

	@override
	int get hashCode => access.hashCode ^ refresh.hashCode;
}
