class User {
  String? role;
  bool? isEmailVerified;
  String? email;
  String? name;
  String? gender;
  String? birthdate;
  String? phone;
  String? status;
  String? id;

  User({
    this.role,
    this.isEmailVerified,
    this.email,
    this.name,
    this.gender,
    this.birthdate,
    this.phone,
    this.status,
    this.id,
  });

  @override
  String toString() {
    return 'User(role: $role, isEmailVerified: $isEmailVerified, email: $email, name: $name, gender: $gender, phone: $phone, status: $status, id: $id)';
  }

  factory User.fromJson(Map<String, dynamic> json) => User(
        role: json['role'] as String?,
        isEmailVerified: json['isEmailVerified'] as bool?,
        email: json['email'] as String?,
        name: json['name'] as String?,
        gender: json['gender'] as String?,
        birthdate: json['birthdate'] as String?,
        phone: json['phone'] as String?,
        status: json['status'] as String?,
        id: json['id'] as String?,
      );

  Map<String, dynamic> toJson() => {
        'role': role,
        'isEmailVerified': isEmailVerified,
        'email': email,
        'name': name,
        'gender': gender,
        'birthdate': birthdate,
        'phone': phone,
        'status': status,
        'id': id,
      };

  @override
  bool operator ==(Object other) {
    if (identical(other, this)) return true;
    return other is User &&
        other.role == role &&
        other.isEmailVerified == isEmailVerified &&
        other.email == email &&
        other.name == name &&
        other.gender == gender &&
        other.birthdate == birthdate &&
        other.phone == phone &&
        other.status == status &&
        other.id == id;
  }

  @override
  int get hashCode =>
      role.hashCode ^
      isEmailVerified.hashCode ^
      email.hashCode ^
      name.hashCode ^
      gender.hashCode ^
      birthdate.hashCode ^
      phone.hashCode ^
      status.hashCode ^
      id.hashCode;
}