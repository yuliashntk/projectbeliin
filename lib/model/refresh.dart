class Refresh {
	String? token;
	String? expires;

	Refresh({this.token, this.expires});

	@override
	String toString() => 'Refresh(token: $token, expires: $expires)';

	factory Refresh.fromJson(Map<String, dynamic> json) => Refresh(
				token: json['token'] as String?,
				expires: json['expires'] as String?,
			);

	Map<String, dynamic> toJson() => {
				'token': token,
				'expires': expires,
			};

	@override
	bool operator ==(Object other) {
		if (identical(other, this)) return true;
		return other is Refresh && 
				other.token == token &&
				other.expires == expires;
	}

	@override
	int get hashCode => token.hashCode ^ expires.hashCode;
}
