class Access {
	String? token;
	String? expires;

	Access({this.token, this.expires});

	@override
	String toString() => 'Access(token: $token, expires: $expires)';

	factory Access.fromJson(Map<String, dynamic> json) => Access(
				token: json['token'] as String?,
				expires: json['expires'] as String?,
			);

	Map<String, dynamic> toJson() => {
				'token': token,
				'expires': expires,
			};

	@override
	bool operator ==(Object other) {
		if (identical(other, this)) return true;
		return other is Access && 
				other.token == token &&
				other.expires == expires;
	}

	@override
	int get hashCode => token.hashCode ^ expires.hashCode;
}
