import 'package:intl/intl.dart';

class DateHelper {
  static String formatDate(DateTime dateTime) {
    return DateFormat('dd/MMM/yyyy').format(dateTime);
  }

  static String formatDateFromString(String dateTime) {
    DateTime date = DateTime.parse(dateTime);
    return DateFormat('dd/MMM/yyyy').format(date);
  }
}
